<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'limowide' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>vrOV.P+1Tr$zX-Y>nd+%=#XQe/$U9Y=P?gpoEQgXEXRo+yV{~MgEo4&A/z(;4n3' );
define( 'SECURE_AUTH_KEY',  '{eN $#!26~4lv(#%<|5rcN/7HV@:IMsOkR<w}?%|=0uEAf4<hYyxS2AMRHGNS{cd' );
define( 'LOGGED_IN_KEY',    'a:G 3R8iIQE}ytxQAfcaY[~qNZ0$m;Q* *}_>y#(<{NqE^aMrbQ%?meR=L+!z72q' );
define( 'NONCE_KEY',        'x8Bp*WGL6,{VX~f#9Z@t|q%7VG5 jvleZBpl&keU)q1`H8lC;o%,,=C D]=5;M6K' );
define( 'AUTH_SALT',        'b70U3Tb|E6y >ea9<zUxurAk*y&djz;8&IQx=}t&$|6(.+6f,RVcDP[xaIlm1Nw7' );
define( 'SECURE_AUTH_SALT', 'r?G-di-%t@Z.!mZdnThsg9eI5iOv8DI#TU_ltkV+&m{74~#uJP3hiezn%t1rpB!|' );
define( 'LOGGED_IN_SALT',   '@KSOGx65%-;8&e~D`[ST^%pGMmA{lIP5#^V=Z|&Ck2Du&z]W2O!|0mfYvFtl%?RH' );
define( 'NONCE_SALT',       '[`8!7Cq HMr4 @sFk@kl`a6 mV|v<RmctAKxt0^2J0vqeOley?.(k!|{I{LYsEI)' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define('WP_HOME', 'https://limowide.com/');
define('WP_SITEURL', 'https://limowide.com/');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
