<?php /*Template Name: Home */ ?>
<?php get_header(); ?>
<?php 
    $section_array = apply_filters("sections_filter", array(
        'home-page/banner-section', 
        'home-page/booking-form', 
        'home-page/features-section', 
        //'home-page/recommended-section', 
        'home-page/our-work-section', 
        'home-page/map-section'
    )); 
    if(!empty($section_array)){
        foreach($section_array as $section){
           get_template_part($section);
        }
    }
?>		
<?php get_footer(); ?>
