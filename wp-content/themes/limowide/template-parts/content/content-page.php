<?php
/**
 * Template part for displaying page content in page.php
 *
 */

?>
<div class="container" style="padding-top: 100px;">
		<div class="row">
			<div class="col-sm-12">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( ! is_front_page() ) : ?>
						<header class="branding_portion">
							<div class="text-center">
								<div class="mb-5">
									<?php get_template_part( 'template-parts/header/entry-header' ); ?>
								</div>
							</div>		
						</header><!-- .entry-header -->	
					<?php elseif ( has_post_thumbnail() ) : ?>
						<header class="entry-header alignwide">
							<?php the_post_thumbnail(); ?>
						</header><!-- .entry-header -->	
					<?php endif; ?>

					<div class="entry-content">
						<?php 
							the_content();
							wp_link_pages(
								array(
									'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'limowide' ) . '">',
									'after'    => '</nav>',
									/* translators: %: Page number. */
									'pagelink' => esc_html__( 'Page %', 'limowide' ),
								)
							);
						?>
					</div>						
				</article><!-- #post-<?php the_ID(); ?> -->
			</div>
		</div>
	</div><!-- .entry-content -->
