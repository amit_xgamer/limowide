 <!-- banner HTML start -->
<section class="banner-section">
    <div class="banner-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?=the_field('banner_content')?>
                    <!-- <div class="explore-btn">
                        <a href="#">Explore Now <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- banner HTML end -->