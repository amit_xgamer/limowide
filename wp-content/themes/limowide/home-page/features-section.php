<!-- site-features HTML start -->
<section class="features">
    <div class="container" data-aos="fade-left">
        <div class="row">
            <div class="col-md-4">
                <div class="box">
                    <div class="feature_heading">
                        <span class="icon">
                            <?php $image1 = get_field('feature_icon_1'); ?>
                            <img src="<?=$image1['url']?>" alt="<?=$image1['alt']?>" />
                        </span>
                        <h3><?=the_field('feature_title_1')?></h3>
                    </div>
                    <p><?=the_field('feature_content_1')?></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <div class="feature_heading">
                        <span class="icon">
                            <?php $image2 = get_field('feature_icon_2'); ?>
                            <img src="<?=$image2['url']?>" alt="<?=$image2['alt']?>" />
                        </span>
                        <h3><?=the_field('feature_title_2')?></h3>
                    </div>
                    <p><?=the_field('feature_content_2')?></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <div class="feature_heading">
                        <span class="icon">
                            <?php $image3 = get_field('feature_icon_3'); ?>
                            <img src="<?=$image3['url']?>" alt="<?=$image3['alt']?>" />
                        </span>
                        <h3><?=the_field('feature_title_3')?></h3>
                    </div>
                    <p><?=the_field('feature_content_3')?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- site-features HTML end -->