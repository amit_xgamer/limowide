<!-- booking-form HTML start -->
<section class="booking-form" data-aos="zoom-in">
    <?php $lang = pll_current_language(); ?>
    <div style="overflow: hidden; max-width: 100%;">
        <iframe scrolling="no" src="https://limowide.com/embed/limowide?lang=<?=$lang?>&currency=" allowtransparency="yes" scrolling="no" frameborder="0" style="border: 0px none; height: 635px; margin-top: -377px; width: 100%;">
        </iframe>
    </div>
</section>
<!-- booking-form HTML end -->