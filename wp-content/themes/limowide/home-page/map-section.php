<!-- our-map HTML start -->
<section class="our_map">
	<div class="container" data-aos="zoom-in">
		<?php $image2 = get_field('map_section_image'); ?>
        <img src="<?=$image2['url']?>" alt="<?=$image2['alt']?>" class="map_bg" />
		<div class="map_texture">
			<h4><?php the_field('map_section_heading'); ?></h4>
			<p><?php the_field('map_section_content'); ?></p>
			<?php the_field('map_section_points'); ?>
			<div class="text-center">						
			</div>
			
		</div>
	</div>
</section>
<!-- our-map HTML end -->