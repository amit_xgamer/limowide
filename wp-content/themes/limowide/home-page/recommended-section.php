 <!-- recommended-car HTML start -->
<section class="recommended">
    <div class="container">
        <div class="thumbsup-header">
			<span class="fa fa-thumbs-o-up"></span>
		</div>
		<div class="recommended-content">
			<h4><?php _e('Recommended Cars', 'limowide'); ?></h4>
			<p><?php _e('We bring some most recommended cars for your lorem ipsum dolor sit ammet is a dummy text which is use for printing text.', 'limowide'); ?></p>
		</div>
        <div class="cars_collection">
            <div class="owl-carousel owl-theme recommended-carousel">
                <?php 
			        $args = array('post_type' => 'recommended_car',  'order' => 'ASC', 'orderBy' =>'id', 'posts_per_page' => -1);
			        $loop = new WP_Query( $args );
			        if($loop->have_posts()):
			          while ( $loop->have_posts() ) : $loop->the_post();
			           $post_id = get_the_ID(); 
			      ?> 

                <div class="item" data-aos="zoom-in">
                    <div class="car_box">
						<h5><?php the_title(); ?></h5>
						<span class="d-block price"><i class="fa fa-inr"></i> <?php the_field('start_price', $post_id); ?> - <i class="fa fa-inr"></i> <?php the_field('end_price', $post_id); ?></span>
						<figure>
							<?php if ( has_post_thumbnail() ) : ?> 
					          	<?php the_post_thumbnail(); ?>
					        <?php endif; ?> 
						</figure>
						<ul class="points">
							<li>
								<span><?php the_field('engine', $post_id); ?></span>
								<small>Engine</small>
							</li>
							<li>
								<span><?php the_field('mileage', $post_id); ?></span>
								<small>Mileage</small>
							</li>
							<li>
								<span><?php the_field('transmission', $post_id); ?></span>
								<small>Transmission</small>
							</li>
						</ul>
					</div>
                </div>
                <?php endwhile; ?>
		      	<?php endif; ?>
		      	<?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>
<!-- recommended-car HTML end -->