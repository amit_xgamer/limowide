        <!-- footer HTML start -->
		<footer>
            <div class="container">
                <div class="footer_top">
                    <a class="footer_logo" href="<?php echo esc_url(site_url('/'))?>"><img src="<?=of_get_option('footer_image')?>" alt="<?=get_bloginfo('title')?>"></a>
                    <p>One Stop Payroll was founded with the goal of providing simple, cost-effective, personalized payroll service for small to medium-sized business.</p>
                </div>
                <div class="footer-box">
                    <div class="row">
                        <div class="col-sm-6">
                            <h6>Company</h6>
                            <?php
                                if( has_nav_menu( 'primary' ) ) {
                                  wp_nav_menu( array( 'theme_location' => 'footer', 'container' => '', 'menu_class' => 'footer_links', 'menu_id' => '') );
                                } 
                            ?>
                        </div>
                       <!--  <div class="col-sm-4">
                            <h6>Solutions</h6>
                            <ul class="footer_links">
                                <li><a href="payroll.html">Payroll</a></li>
                                <li><a href="time-and-attendance.html">Time and Attendance</a></li>
                                <li><a href="hr.html">HR</a></li>
                                <li><a href="employee-benefit.html">Benefits Enrollment</a></li>
                                <li><a href="workers-comp.html">Workers Comp</a></li>
                                <li><a href="talent-acquistion.html">Talent Acquisition</a></li>
                            </ul>
                        </div> -->
                        <div class="col-sm-6 corporate_hq">
                           <?php echo of_get_option( 'address'); ?>
                            <ul class="contact_links">
                                <li>
                                    <a href="callto: <?php echo of_get_option( 'phone'); ?>"><i class="fa fa-phone"></i><?php echo of_get_option( 'phone'); ?></a>
                                </li>
                                <li>
                                    <a href="callto: <?php echo of_get_option( 'alt_phone'); ?>"><i class="fa fa-phone"></i><?php echo of_get_option( 'alt_phone'); ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <ul class="social-links">
                        <li>
                            <a href="<?php echo of_get_option( 'sn_link1'); ?>"><i class="fa fa-<?php echo of_get_option( 'sn_icon1'); ?>"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo of_get_option( 'sn_link2'); ?>"><i class="fa fa-<?php echo of_get_option( 'sn_icon2'); ?>"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo of_get_option( 'sn_link3'); ?>"><i class="fa fa-<?php echo of_get_option( 'sn_icon3'); ?>"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo of_get_option( 'sn_link4'); ?>"><i class="fa fa-<?php echo of_get_option( 'sn_icon4'); ?>"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="copyright">
                <div class="container">
                   <?php echo of_get_option( 'copyright'); ?>
                </div>
            </div>
        </footer>
		<!-- footer HTML end -->
        </div>
        <?php wp_footer(); ?>
        <script>
            $(function () {
				AOS.init(); 
            });
        </script>
        <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

            $('#data_type').change(function(){
                if ($(this).prop('checked')) {
                    $('.data_type_text').text('Agency');
                }
                else {
                    $('.data_type_text').text('Travel Agent');
                }
            })

            $('.btn_show').click(function(){
                $(this).prev('.more_actions').removeClass('d-none');
                $(this).parents('td').children('.btn_hide').addClass('d-inline-block');
            })

            $('.btn_hide').click(function(){
                $(this).removeClass('d-inline-block');
                $(this).parents('td').children('.more_actions').addClass('d-none');
            })
            
            $('.pll-parent-menu-item').hover(function() {
                $(this).find('.sub-menu').show();
            }, function() {
                $(this).find('.sub-menu').hide();
            })
        });
    </script>
    </body>
</html>