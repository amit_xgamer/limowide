<?php /*Template Name: How It Works */ ?>
<?php get_header(); ?>
<div class="how_it_works_page">

	<div class="branding_portion">
		<div class="text-center">
			<div class="mb-5">
				<h2><span><?php the_field('page_title'); ?></span></h2>
			</div>

			<div class="working_divs">
				<?php the_field('page_content_1'); ?>
			</div>

			<figure class="laptop_image">
				<?php $image1 = get_field('page_image_1'); ?>
				<img src="<?php echo $image1['url']?>" alt="<?php echo $image1['alt']?>" class="w-100" />
			</figure>

			<div class="working_divs ride_detail">
				<div>
					<?php the_field('page_content_2'); ?>
				</div>
			</div>

			<figure class="laptop_image">
				<?php $image2 = get_field('page_image_2'); ?>
				<img src="<?php echo $image2['url']?>" alt="<?php echo $image2['alt']?>" class="w-100" />
			</figure>

			<div class="working_divs fedback_div">
				<?php the_field('page_content_3'); ?>
			</div>
		</div>
	</div>

</div>
<?php get_footer(); ?>