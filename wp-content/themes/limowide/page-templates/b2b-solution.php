<?php /*Template Name: B2B Solution */ ?>
<?php get_header(); ?>
<!-- how-it-works HTML start -->
		<div class="banner_section" style="background: url('https://app-prod.moziogroup.com/20200722-1243/_next/static/images/476bdbbd712c86f20775c523410c7485.jpg');">
			<h3>Unlock Mobility’s Potential</h3>
			<p>Mozio partners with travel, mapping and transportation companies to make rideshare, taxis, limos, shuttles, buses and trains bookable through every channel.</p>
		</div>

		<div class="how_it_workss_texture">
			<h4>Trusted by the world’s biggest travel companies:</h4>
			<div class="partner-list">
				<ul class="">
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-1.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-2.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-3.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-4.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-5.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-6.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-7.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-8.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-9.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-10.PNG" alt="list item" /></li>
				</ul>
			</div>
			<hr style="max-width: 800px;margin: 45px auto 65px;border-width: 5px;">

			<h5>Mozio Capabilities</h5>

			<div class="how-it_works_links">
				<div class="container">
					<div class="tab_list">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
							  <a class="active" data-toggle="tab" href="#tab_1">White-Labeled Booking Platform</a>
							</li>
							<li class="nav-item">
							  <a class="" data-toggle="tab" href="#tab_2">Mobile SDK</a>
							</li>
							<li class="nav-item">
							  <a class="" data-toggle="tab" href="#tab_3">Single-Source Booking API</a>
							</li>
							<li class="nav-item">
							  <a class="" data-toggle="tab" href="#tab_4">Dynamic Packaging Integration</a>
							</li>
							<li class="nav-item">
							  <a class="" data-toggle="tab" href="#tab_5">Mobile Ticketing</a>
							</li>
							<li class="nav-item">
							  <a class="" data-toggle="tab" href="#tab_6">Tracking Framework</a>
							</li>
						  </ul>
					</div>
					
					  <!-- Tab panes -->
						<div class="tab-content">
							<div id="tab_1" class="tab-pane active">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>White-Labeled Booking Platform</h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<picture>
											  <img src="https://app-prod.moziogroup.com/20200722-1243/_next/static/images/3697f890afd30a4b03e07e097274db58.gif" alt="computer" class="w-100" />
											</picture>
										</div>
									</div>
								</div>
							</div>
							<div id="tab_2" class="tab-pane fade">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>qwqwqWhite-Labeled Booking Platform</h3>
												<p>21212Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<picture>
											  <img src="https://app-prod.moziogroup.com/20200722-1243/_next/static/images/3697f890afd30a4b03e07e097274db58.gif" alt="computer" class="w-100" />
											</picture>
										</div>
									</div>
								</div>
							</div>
							<div id="tab_3" class="tab-pane fade">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>White-Labeled Booking Platform</h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<picture>
											  <img src="https://app-prod.moziogroup.com/20200722-1243/_next/static/images/3697f890afd30a4b03e07e097274db58.gif" alt="computer" class="w-100" />
											</picture>
										</div>
									</div>
								</div>
							</div>
							<div id="tab_4" class="tab-pane fade">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>White-Labeled Booking Platform</h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<picture>
											  <img src="https://app-prod.moziogroup.com/20200722-1243/_next/static/images/3697f890afd30a4b03e07e097274db58.gif" alt="computer" class="w-100" />
											</picture>
										</div>
									</div>
								</div>
							</div>
							<div id="tab_5" class="tab-pane fade">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>White-Labeled Booking Platform</h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<picture>
											  <img src="https://app-prod.moziogroup.com/20200722-1243/_next/static/images/3697f890afd30a4b03e07e097274db58.gif" alt="computer" class="w-100" />
											</picture>
										</div>
									</div>
								</div>
							</div>
							<div id="tab_6" class="tab-pane fade">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>White-Labeled Booking Platform</h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<picture>
											  <img src="https://app-prod.moziogroup.com/20200722-1243/_next/static/images/3697f890afd30a4b03e07e097274db58.gif" alt="computer" class="w-100" />
											</picture>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
			
			<hr style="max-width: 800px;margin: 45px auto 65px;border-width: 5px;">
			
			<h5>Get Moving with Mozio</h5>
			
			
			<div class="how-it_works_links mt-5">
				<div class="container">
					<div class="tab_list">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
							  <a class="active" data-toggle="tab" href="#movetab_1">Airlines</a>
							</li>
							<li class="nav-item">
							  <a class="" data-toggle="tab" href="#movetab_2">OTAs</a>
							</li>
							<li class="nav-item">
							  <a class="" data-toggle="tab" href="#movetab_3">Travel Agencies</a>
							</li>
							<li class="nav-item">
							  <a class="" data-toggle="tab" href="#movetab_4">Hotel Groups</a>
							</li>
							<li class="nav-item">
							  <a class="" data-toggle="tab" href="#movetab_5">Corporate Travel</a>
							</li>
							<li class="nav-item">
							  <a class="" data-toggle="tab" href="#movetab_6">Transportation Providers</a>
							</li>
						  </ul>
					</div>
					
					  <!-- Tab panes -->
						<div class="tab-content">
							<div id="movetab_1" class="tab-pane active">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>Airlines</h3>
												<p>Airlines use Mozio's booking platform to allow their customers to find and book rides from and to the airport, enhancing the customer experience.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<ul class="list-inline airline_tabs">
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-1.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-2.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-3.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-4.PNG" class="w-100" /></span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div id="movetab_2" class="tab-pane fade">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>Airlines</h3>
												<p>Airlines use Mozio's booking platform to allow their customers to find and book rides from and to the airport, enhancing the customer experience.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<ul class="list-inline airline_tabs">
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-1.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-2.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-3.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-4.PNG" class="w-100" /></span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div id="movetab_3" class="tab-pane fade">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>Airlines</h3>
												<p>Airlines use Mozio's booking platform to allow their customers to find and book rides from and to the airport, enhancing the customer experience.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<ul class="list-inline airline_tabs">
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-1.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-2.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-3.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-4.PNG" class="w-100" /></span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div id="movetab_4" class="tab-pane fade">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>Airlines</h3>
												<p>Airlines use Mozio's booking platform to allow their customers to find and book rides from and to the airport, enhancing the customer experience.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<ul class="list-inline airline_tabs">
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-1.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-2.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-3.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-4.PNG" class="w-100" /></span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div id="movetab_5" class="tab-pane fade">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>Airlines</h3>
												<p>Airlines use Mozio's booking platform to allow their customers to find and book rides from and to the airport, enhancing the customer experience.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<ul class="list-inline airline_tabs">
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-1.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-2.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-3.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-4.PNG" class="w-100" /></span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div id="movetab_6" class="tab-pane fade">
								<div class="list_data">
									<div class="row">
										<div class="col-sm-7">
											<div class="b2b_texture">
												<h3>Airlines</h3>
												<p>Airlines use Mozio's booking platform to allow their customers to find and book rides from and to the airport, enhancing the customer experience.</p>
											</div>
										</div>
										<div class="col-sm-5">
											<ul class="list-inline airline_tabs">
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-1.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-2.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-3.PNG" class="w-100" /></span></li>
												<li><span class="icons_inner"><img src="<?php echo get_template_directory_uri()?>/img/partner-4.PNG" class="w-100" /></span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
			
			<hr style="max-width: 800px;margin: 60px auto 65px;border-width: 5px;">
			<h5>Modes of Transportation</h5>
			<p style="max-width: 800px;margin: auto;text-align: center;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in</p>
			<div class="partner-list">
				<ul class="">
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-1.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-2.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-3.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-4.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-5.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-6.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-7.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-8.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-9.PNG" alt="list item" /></li>
					<li class="list-inline-items"><img src="<?php echo get_template_directory_uri()?>/img/partner-10.PNG" alt="list item" /></li>
				</ul>
			</div>
		</div>
		<!-- how-it-works HTML end -->
<?php get_footer(); ?>