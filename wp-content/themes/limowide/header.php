<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title><?php wp_title( '|', true, 'right' ); ?></title>        
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo of_get_option( 'uploader_favicon'); ?>">
        <?php wp_head(); ?>    
    </head>

    <body <?php body_class(); ?>>
    	<?php $inner_class = (is_home() || is_front_page()) ? '' : 'inner_header'; ?>
        <!-- header HTML start-->
        <header id="desktop_header" class="<?=$inner_class?>">
            <nav class="navbar navbar-expand-md navbar-dark fixed-top navigation">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo esc_url(site_url('/'))?>"><img src="<?=of_get_option('uploader_logo')?>" alt="<?=get_bloginfo('title')?>"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarCollapse">
                        <?php
                            if( has_nav_menu( 'primary' ) ) {
                              wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'menu_class' => 'navbar-nav ml-auto', 'menu_id' => 'nav') );
                            } 
                        ?>
                       <!--  <div class="dropdown language-dd">
                            <button type="button" class="btn btn-link" data-toggle="dropdown"><i class="fa fa-inr mr-2"></i> INR</button>
                            <div class="dropdown-menu">
								<span href="javascript: void(0);" style="display: block;border-bottom: 1px solid #ccc;padding: 10px 15px;font-size: 15px;text-align: left;margin-bottom: 10px;">Currency</span>
                                <a class="dropdown-item" href="#">INR</a>
                                <a class="dropdown-item" href="#">USD</a>
                                <a class="dropdown-item" href="#">EUR</a>
                            </div>
                        </div> -->
                        <!-- <button class="btn my-2 my-sm-0 sign-btn">Sign In</button> -->
                        <div class="dropdown language-dd">
							<iframe src="https://limowide.com/frame/account" frameborder="0" allowtransparency="yes" scrolling="no" style="width:100%; height:68px; position:relative;top:26px;margin:0;padding:0;" ></iframe>
                            <!-- <button type="button" class="btn btn-link dropdown-toggle en-dropdown" data-toggle="dropdown"><i class="flag-icon"><img src="img/flag.png" /></i>En</button>
                            <div class="dropdown-menu">
								<span href="javascript: void(0);" style="display: block;border-bottom: 1px solid #ccc;padding: 10px 15px;font-size: 15px;text-align: left;margin-bottom: 10px;">Language & Country</span>
								<a class="dropdown-item" href="#"><i class="flag-icon"><img src="img/flag.png" /></i> Link 1</a>
								<a class="dropdown-item" href="#"><i class="flag-icon"><img src="img/flag.png" /></i> Link 2</a>
								<a class="dropdown-item" href="#"><i class="flag-icon"><img src="img/flag.png" /></i> Link 3</a>
							</div> -->
                        </div>
						
						<div class="dropdown language-dd">
                        	<?php // pll_the_languages(['dropdown' => 1, 'show_flags' => 1, 'echo' => 1]); ?>
							<!-- <iframe src="https://limowide.com/frame/account" frameborder="0" width="100%" ></iframe> -->
							<!-- <button class="btn my-2 my-sm-0 login-btn dropdown-toggle en-dropdown" data-toggle="dropdown"><i class="fa fa-user mr-2" style="font-size: 21px;"></i> Account</button>
                            <div class="dropdown-menu">
								<a class="dropdown-item" href="#">Sign In</a>
								<a class="dropdown-item" href="#">Register</a>
							</div> -->
                        </div>
                    </div>
                </div>
            </nav>
        </header>
        <!-- header HTML end-->

        <!-- mobile-header HTML start -->
        <header id="mobile_header">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="img/logo.png" /></a>
                <div class="right_menu">
                    <div class="dropdown language-dd">
                        <button type="button" class="btn btn-link" data-toggle="dropdown"><i class="fa fa-inr mr-2"></i> INR</button>
                        <div class="dropdown-menu">
                            <span href="javascript: void(0);" style="display: block;border-bottom: 1px solid #ccc;padding: 10px 15px;font-size: 15px;text-align: left;margin-bottom: 10px;">Currency</span>
                            <a class="dropdown-item" href="#">INR</a>
                            <a class="dropdown-item" href="#">USD</a>
                            <a class="dropdown-item" href="#">EUR</a>
                        </div>
                    </div>
                    <!-- <button class="btn my-2 my-sm-0 sign-btn">Sign In</button> -->
                    <div class="dropdown language-dd flag-dd">
                        <button type="button" class="btn btn-link dropdown-toggle en-dropdown" data-toggle="dropdown"><i class="flag-icon"><img src="img/flag.png" /></i>En</button>
                        <div class="dropdown-menu">
                            <span href="javascript: void(0);" style="display: block;border-bottom: 1px solid #ccc;padding: 10px 15px;font-size: 15px;text-align: left;margin-bottom: 10px;">Language & Country</span>
                            <a class="dropdown-item" href="#"><i class="flag-icon"><img src="img/flag.png" /></i> Link 1</a>
                            <a class="dropdown-item" href="#"><i class="flag-icon"><img src="img/flag.png" /></i> Link 2</a>
                            <a class="dropdown-item" href="#"><i class="flag-icon"><img src="img/flag.png" /></i> Link 3</a>
                        </div>
                    </div>
                    
                    <div class="dropdown language-dd account_dd">
                        <button class="btn  login-btn dropdown-toggle en-dropdown" data-toggle="dropdown"><i class="fa fa-user mr-2" style="font-size: 21px;"></i> <span>Account</span></button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Sign In</a>
                            <a class="dropdown-item" href="#">Register</a>
                        </div>
                    </div>

                    <button type="button" class="mobile_toggler"><i class="fa fa-bars"></i></button>
                </div>
            </div>
        </header>
        <!-- mobile-header HTML end -->

        <!-- mobile_menu HTML start -->       
         <?php
            if( has_nav_menu( 'primary' ) ) {
              wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'menu_class' => 'mobile_menu', 'menu_id' => 'nav') );
            } 
        ?>
        <!-- mobile_menu HTML end -->

        <div class="wrapper">