<?php

 /**
  * A unique identifier is defined to store the options in the database and reference them from the theme.
  * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
  * If the identifier changes, it'll appear as if the options have been reset.
  */
 function optionsframework_option_name() {

     // This gets the theme name from the stylesheet
     $themename = wp_get_theme();
     $themename = preg_replace("/\W/", "_", strtolower($themename));

     $optionsframework_settings = get_option('optionsframework');
     $optionsframework_settings['id'] = $themename;
     update_option('optionsframework', $optionsframework_settings);
 }

 /**
  * Defines an array of options that will be used to generate the settings page and be saved in the database.
  * When creating the 'id' fields, make sure to use all lowercase and no spaces.
  *
  * If you are making your theme translatable, you should replace 'limowide'
  * with the actual text domain for your theme.  Read more:
  * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
  */
 function optionsframework_options() {

    // Background Defaults
    $background_color = '#fff';

    $background_defaults = array(
  	 'color' => '#f9f6f1',
  	 'image' => get_stylesheet_directory_uri() . '/images/body-bg.jpg',
  	 'repeat' => 'repeat',
  	 'position' => 'left top',
  	 'attachment' => 'scroll'
    );

     // Pull all the categories into an array
     $options_categories = array();
     $options_categories_obj = get_categories();
     foreach ($options_categories_obj as $category) {
	     $options_categories[$category->cat_ID] = $category->cat_name;
     }

     // Pull all the posts into an array
     $options_posts = array();
     $options_posts_obj = get_posts();
     $options_posts[''] = 'Select a post:';
     foreach ($options_posts_obj as $post) {
	     $options_posts[$post->ID] = $post->post_title;
     }

     $options = array();

    $wp_editor_settings = array(
      'wpautop' => true, // Default
      'textarea_rows' => 5,
      'tinymce' => array( 'plugins' => 'wordpress,wplink' )
    );


  $options[] = array(
	 'name' => __('Basic Settings', 'limowide'),
	 'type' => 'heading');
     
  $options[] = array(
	 'name' => __('Theme Logo', 'limowide'),
	 'desc' => __('Upload an image to be displayed as Logo', 'limowide'),
	 'id' => 'uploader_logo',
	 'type' => 'upload');

  $options[] = array(
   'name' => __('Footer Image', 'limowide'),
   'desc' => __('Upload an image to be displayed as footer logo', 'limowide'),
   'id' => 'footer_image',
   'type' => 'upload');

  $options[] = array(
	 'name' => __('Favicon', 'limowide'),
	 'desc' => __('Upload an image to be displayed as Logo', 'limowide'),
	 'id' => 'uploader_favicon',
	 'type' => 'upload');
     
    $options[] = array(
  	 'name' => __('Copyright Text', 'limowide'),
  	 'desc' => __('Footer copyright text ', 'limowide'),
  	 'id' => 'copyright',
  	 'type' => 'text'
    );

    /*******************Social Icons*************************/
    $options[] = array(
  	 'name' => __('Social Icons', 'limowide'),
  	 'type' => 'heading'
    );

    $options[] = array(
      'name' => __('Social Networking Icon #1', 'limowide'),
      'desc' => __('Add Font awesome icon text', 'limowide'),
      'id' => 'sn_icon1',
      'type' => 'text'
    );
    $options[] = array(
    	'name' => __('Social Networking Link #1', 'limowide'),
    	'desc' => __('Enter Social Networking Link', 'limowide'),
    	'id' => 'sn_link1',
    	'type' => 'text'
    );

    $options[] = array(
      'name' => __('Social Networking Icon #2', 'limowide'),
      'desc' => __('Add Font awesome icon text', 'limowide'),
      'id' => 'sn_icon2',
      'type' => 'text'
    );
    $options[] = array(
      'name' => __('Social Networking Link #2', 'limowide'),
      'desc' => __('Enter Social Networking Link', 'limowide'),
      'id' => 'sn_link2',
      'type' => 'text'
    );

    $options[] = array(
      'name' => __('Social Networking Icon #3', 'limowide'),
      'desc' => __('Add Font awesome icon text', 'limowide'),
      'id' => 'sn_icon3',
      'type' => 'text'
    );
    $options[] = array(
      'name' => __('Social Networking Link #3', 'limowide'),
      'desc' => __('Enter Social Networking Link', 'limowide'),
      'id' => 'sn_link3',
      'type' => 'text'
    );

     $options[] = array(
      'name' => __('Social Networking Icon #4', 'limowide'),
      'desc' => __('Add Font awesome icon text', 'limowide'),
      'id' => 'sn_icon4',
      'type' => 'text'
    );
    $options[] = array(
      'name' => __('Social Networking Link #4', 'limowide'),
      'desc' => __('Enter Social Networking Link', 'limowide'),
      'id' => 'sn_link4',
      'type' => 'text'
    );

     $options[] = array(
      'name' => __('Social Networking Icon #5', 'limowide'),
      'desc' => __('Add Font awesome icon text', 'limowide'),
      'id' => 'sn_icon5',
      'type' => 'text'
    );
    $options[] = array(
      'name' => __('Social Networking Link #5', 'limowide'),
      'desc' => __('Enter Social Networking Link', 'limowide'),
      'id' => 'sn_link5',
      'type' => 'text'
    );

    /*******************Contact Details*************************/
    $options[] = array(
     'name' => __('Contact Details', 'limowide'),
     'type' => 'heading'
    );

    $options[] = array(
      'name' => __( 'Contact Address', 'limowide' ), 
      'id' => 'address',
      'type' => 'editor',
      'settings' => $wp_editor_settings
    );

    $options[] = array(
      'name' => __('Email Address', 'limowide'),
      'desc' => __('Enter email address', 'limowide'),
      'id' => 'email',
      'type' => 'text'
    );

    $options[] = array(
      'name' => __('Phone Number', 'limowide'),
      'desc' => __('Enter phone Number', 'limowide'),
      'id' => 'phone',
      'type' => 'text'
    );

    $options[] = array(
      'name' => __('Alternate Number', 'limowide'),
      'desc' => __('Enter Alternate Number', 'limowide'),
      'id' => 'alt_phone',
      'type' => 'text'
    );

    return $options;
 }
 