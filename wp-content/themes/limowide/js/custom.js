$(function () {
	$(window).on("scroll", function () {
		if ($(window).scrollTop() > 50) {
			$(".navbar").addClass("active-header");
		} else {
			//remove the background property so it comes transparent again (defined in your css)
			$(".navbar").removeClass("active-header");
		}
	});

	// mobile_header
	$(window).on("scroll", function () {
		if ($(window).scrollTop() > 50) {
			$("#mobile_header").addClass("fixed-mob-header");
		} else {
			//remove the background property so it comes transparent again (defined in your css)
			$("#mobile_header").removeClass("fixed-mob-header");
		}
	});

	// mobile menu js start
	$('.mobile_toggler').click(function(){
		$('.mobile_menu').slideToggle();
	});

	// bifrost_sidebar open
	$('.btn_bifrost_nav').click(function(){
		$('.bifrost_navigation').toggleClass('show_bifrost_sidebar');
		$(this).toggleClass('rotate_icon');
	});

	// datepicker
	$(".datepicker").datepicker({
		showOn: "button",
		buttonImage: "img/calendar.svg",
		buttonImageOnly: true,
		buttonText: "Select date",
	});

	// timepicker
	$('.clockpicker').clockpicker({
		twelvehour: true,
		placement: 'top',
		align: 'left',
		donetext: 'Done'
	});

	// recommended-carousel
	$('.recommended-carousel').owlCarousel({
		loop: true,
		margin: 20,
		dots: false,
		nav: true,
		center: false,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 2
			},
			1000: {
				items: 3
			},
			1100: {
				items: 4
			}
		}
	})
	// searchfield
	$('.search_field').keyup(function () {

		if ($(this).val() == '') {
			$(this).parent('.from_result').children('.search-list').fadeOut();
		}
		else {
			// $('.from_result').children('.search-list').fadeOut();
			$(this).parent('.from_result').children('.search-list').fadeIn();
		}

	})

	$('.btn_destination').click(function () {
		$('.destination_sidebar').addClass('show_destination_sidebar');
	});
	$('.btn_sidebar_close').click(function () {
		$('.destination_sidebar').removeClass('show_destination_sidebar');
	});
	$('.btn_booknow').on('click', function () {
		$('body , html').addClass('show_booknow');
	})


	// line chart
	var ctx = document.getElementById("vacancylocation_chart");
	var lineChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: ["0-1", "1-3", "3-6", "6-10", "10-13", "13-16", "16-20"],
			datasets: [{
				label: 'Prerequisite Standard',
				data: [
					30, 25, 50, 80, 55, 96, 76, 45
				],
				backgroundColor: 'transparent',
				borderColor: '#172B4D',
				borderWidth: 2
			}]
		},
		options: {
			legend: {
				display: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					ticks: {
						display: true,
					},
					gridLines: {
						drawOnChartArea: false
					},
					display: true
				}],
				yAxes: [{
					ticks: {
						display: true,
					},
					gridLines: {
						drawOnChartArea: false
					},
					display: true
				}]
			}
		}
	});
});
